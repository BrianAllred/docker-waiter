package main

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

func main() {
	if _, err := getConfig(); err != nil {
		panic(err)
	}

	err := initDocker()
	if err != nil {
		panic(err)
	}

	if len(config.ContainerConfigs) <= 0 {
		panic(errors.New("no container configurations defined"))
	}

	c := make(chan error, len(config.ContainerConfigs))

	for _, config := range config.ContainerConfigs {
		go containerCheckLoop(config, c)
	}

	for err := range c {
		panic(err)
	}
}

func containerCheckLoop(config *containerConfig, c chan error) {
	mainIsHealthy := false

	err := config.setupConfig()
	if err != nil {
		c <- err
		return
	}

	dependentContainers := make([]string, 0)
	for _, container := range config.DepContainers {
		dependentContainers = append(dependentContainers, "/"+container)
	}

	mainContainerName := "/" + config.MainContainer

	for {
		mainHealthStatus, err := containerGetHealthStatusByName(mainContainerName)
		if err != nil {
			fmt.Printf("Container %s could not be found, setting to unhealthy...\n", config.MainContainer)
			mainIsHealthy = false
		}

		mainHealthStatusLower := strings.ToLower(mainHealthStatus)

		if mainHealthStatusLower == "healthy" || (config.CheckRunning && mainHealthStatusLower == "running") {
			if !mainIsHealthy {
				fmt.Printf("%s healthy, starting containers...\n", mainContainerName[1:])
				mainIsHealthy = true
			}

			for _, container := range dependentContainers {
				containerRunning, err := containerGetRunningByName(container)
				if err != nil {
					fmt.Printf("Container %s could not be found, not stopping...\n", container[1:])
					continue
				}

				if !containerRunning {
					err := containerStartByName(container)
					if err != nil {
						c <- err
						return
					}

					fmt.Printf("Starting container %s...\n", container[1:])
				}
			}
		} else {
			if mainIsHealthy {
				fmt.Printf("%s stopped, starting, or unhealthy; stopping containers...\n", mainContainerName[1:])
				mainIsHealthy = false
			}

			for _, container := range dependentContainers {
				containerRunning, err := containerGetRunningByName(container)
				if err != nil {
					fmt.Printf("Container %s could not be found, not stopping...\n", container[1:])
					continue
				}

				if containerRunning {
					err := containerStopByName(container)
					if err != nil {
						c <- err
						return
					}

					fmt.Printf("Stopping container %s...\n", container[1:])
				}
			}
		}

		time.Sleep(config.checkDelayDuration())
	}
}
