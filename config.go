package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path"
	"time"
)

type Config struct {
	ContainerConfigs []*containerConfig `json:",omitempty"`
	DockerConfig     *dockerConfig      `json:",omitempty"`
}

type containerConfig struct {
	MainContainer string   `json:",omitempty"`
	DepContainers []string `json:",omitempty"`
	CheckRunning  bool     `json:",omitempty"`
	CheckDelay    float64  `json:",omitempty"`
}

type dockerConfig struct {
	Host       string `json:",omitempty"`
	APIVersion string `json:",omitempty"`
	CertPath   string `json:",omitempty"`
	TLSVerify  bool   `json:",omitempty"`
}

var config *Config

func newConfig() *Config {
	config := &Config{}
	return config
}

func getConfig() (*Config, error) {
	if config == nil {
		configFile, err := getConfigFile()
		if err != nil {
			return nil, err
		}

		if len(configFile) <= 0 {
			return newConfig(), nil
		}

		err = json.Unmarshal(configFile, &config)
		if err != nil {
			return nil, err
		}
	}

	return config, nil
}

func getConfigFilePath() (string, error) {
	configFilePath := "/etc/docker-waiter/config.json"

	if os.Geteuid() != 0 {
		xdgHome := os.Getenv("XDG_CONFIG_HOME")
		if len(xdgHome) > 0 {
			configFilePath = path.Join(xdgHome, "docker-waiter", "config.json")
		} else {
			configFilePath = path.Join(os.Getenv("HOME"), ".config", "docker-waiter", "config.json")
		}
	}

	_, err := os.Stat(configFilePath)

	if os.IsNotExist(err) {
		return "", nil
	}

	if err != nil {
		return "", err
	}

	return configFilePath, nil
}

func getConfigFile() ([]byte, error) {
	configFilePath, err := getConfigFilePath()
	if err != nil {
		return nil, err
	}

	if len(configFilePath) <= 0 {
		return nil, nil
	}

	return ioutil.ReadFile(configFilePath)
}

func (config *containerConfig) setupConfig() error {
	if len(config.MainContainer) <= 0 {
		return errors.New("main container value must exist")
	}

	if len(config.DepContainers) <= 0 {
		return errors.New("dependent container values must exist")
	}

	if config.CheckDelay == 0 {
		config.CheckDelay = 1
	}

	return nil
}

func (config *containerConfig) checkDelayDuration() time.Duration {
	return time.Duration(config.CheckDelay) * time.Second
}
