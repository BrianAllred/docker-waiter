package main

import (
	"context"
	"errors"
	"net/http"
	"path/filepath"

	"docker.io/go-docker"
	"docker.io/go-docker/api/types"
	"github.com/docker/go-connections/tlsconfig"
)

var dockerClient *docker.Client

func initDocker() error {
	var err error

	if dockerClient != nil {
		dockerClient.Close()
	}

	if config.DockerConfig == nil {
		dockerClient, err = docker.NewEnvClient()
		return err
	}

	var client *http.Client
	if len(config.DockerConfig.CertPath) > 0 {
		options := tlsconfig.Options{
			CAFile:             filepath.Join(config.DockerConfig.CertPath, "ca.pem"),
			CertFile:           filepath.Join(config.DockerConfig.CertPath, "cert.pem"),
			KeyFile:            filepath.Join(config.DockerConfig.CertPath, "key.pem"),
			InsecureSkipVerify: config.DockerConfig.TLSVerify,
		}

		tlsc, err := tlsconfig.Client(options)
		if err != nil {
			return err
		}

		client = &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: tlsc,
			},
			CheckRedirect: docker.CheckRedirect,
		}
	}

	dockerClient, err = docker.NewClient(config.DockerConfig.Host, config.DockerConfig.APIVersion, client, nil)
	return err
}

func getIDFromName(containerName string) string {
	containers, err := dockerClient.ContainerList(context.Background(), types.ContainerListOptions{All: true})
	if err != nil {
		panic(err)
	}

	var containerID string

	for _, container := range containers {
		for _, name := range container.Names {
			if name == containerName {
				containerID = container.ID
				break
			}
		}

		if len(containerID) > 0 {
			break
		}
	}

	return containerID
}

func containerStop(containerID string) error {
	return dockerClient.ContainerStop(context.Background(), containerID, nil)
}

func containerStopByName(containerName string) error {
	return containerStop(getIDFromName(containerName))
}

func containerStart(containerID string) error {
	return dockerClient.ContainerStart(context.Background(), containerID, types.ContainerStartOptions{})
}

func containerStartByName(containerName string) error {
	return containerStart(getIDFromName(containerName))
}

func containerGetHealthStatus(containerID string) (string, error) {
	containerStatus, err := dockerClient.ContainerInspect(context.Background(), containerID)
	if err != nil {
		return "", err
	}

	if containerStatus.ContainerJSONBase != nil {
		if containerStatus.ContainerJSONBase.State != nil {
			if containerStatus.ContainerJSONBase.State.Health != nil {
				return containerStatus.ContainerJSONBase.State.Health.Status, nil
			}

			return containerStatus.ContainerJSONBase.State.Status, nil
		}

		return "", errors.New("state not available")
	}

	return "", errors.New("container status not available")
}

func containerGetHealthStatusByName(containerName string) (string, error) {
	return containerGetHealthStatus(getIDFromName(containerName))
}

func containerGetRunning(containerID string) (bool, error) {
	containerStatus, err := dockerClient.ContainerInspect(context.Background(), containerID)
	if err != nil {
		return false, err
	}

	return containerStatus.ContainerJSONBase.State.Running, nil
}

func containerGetRunningByName(containerName string) (bool, error) {
	return containerGetRunning(getIDFromName(containerName))
}
